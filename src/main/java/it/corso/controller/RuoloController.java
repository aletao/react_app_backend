package it.corso.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import it.corso.dto.CreateRuoloDto;
import it.corso.dto.RuoloDto;
import it.corso.service.RuoloService;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@Path("/ruolo")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class RuoloController {

	@Autowired
	private RuoloService ruoloService;
	
	
	@POST
	public Response createRuolo(CreateRuoloDto createRuoloDto) {
		try {
		RuoloDto newRuoloDto = ruoloService.createRuolo(createRuoloDto);
		
		return Response.ok().entity(newRuoloDto).build();
		} catch(Exception e) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}
	
	@GET
	public Response getAll() {
		try {
			List<RuoloDto> listRuoliDto = ruoloService.getAll();
			
			return Response.ok().entity(listRuoliDto).build();
		} catch(Exception e) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}
}
