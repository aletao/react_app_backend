package it.corso.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import it.corso.dto.CorsoDto;
import it.corso.dto.CreateCorsoDto;
import it.corso.dto.UtenteDto;
import it.corso.exceptions.EntityNotFoundException;
import it.corso.jwt.JWTTokenNeeded;
import it.corso.jwt.Secured;
import it.corso.service.CorsoService;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@Secured(role = "Admin")
@JWTTokenNeeded()
@Path("/corso")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class CorsoController {

	@Autowired
	private CorsoService corsoService;
	
	@POST
	public Response createCorso(CreateCorsoDto createCorsoDto) { //catch customexception
		CorsoDto corsoDto = corsoService.createCorso(createCorsoDto);
		
		return Response.ok().entity(corsoDto).build();
	}

	 
	@GET
	@Path("/get/all")
	public Response getCorsi() {
		List<CorsoDto> corsi = corsoService.getCorsi();
		return Response.ok().entity(corsi).build();
	}
	
	
	@GET
	@Path("{idCorso}/users")
	public Response getUtenti(@PathParam("idCorso") Integer idCorso, @QueryParam("idRuolo") Integer idRuolo) {
		try {
			List<UtenteDto> utenti = corsoService.getUtenti(idCorso, idRuolo);
			return Response.ok().entity(utenti).build();
		} catch (EntityNotFoundException e) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
	}
	
	@GET
	public Response searchCorsiByCategoria(@QueryParam("idCategoria") Integer idCategoria) {
		try {
			List<CorsoDto> corsiDto = corsoService.findCorsiByIdCategoria(idCategoria);
			return Response.ok().entity(corsiDto).build();
		} catch(Exception e) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}
}