package it.corso.controller;

import java.security.Key;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import it.corso.dto.CorsoDto;
import it.corso.dto.RuoloDto;
import it.corso.dto.UtenteAggiornamentoDto;
import it.corso.dto.UtenteDto;
import it.corso.dto.UtenteLoginRequestDto;
import it.corso.dto.UtenteLoginResponseDto;
import it.corso.dto.UtenteRegistrazioneDto;
import it.corso.exceptions.EntityNotFoundException;
import it.corso.jwt.JWTTokenNeeded;
import it.corso.jwt.Secured;
import it.corso.model.Ruolo;
import it.corso.model.Utente;
import it.corso.service.UtenteService;
import jakarta.validation.Valid;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@Path("/user")
public class UserController {

    @Autowired
    private UtenteService utenteService;

    @POST
    @Path("/register")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response userRegistration(@Valid @RequestBody UtenteRegistrazioneDto utenteRegistrazioneDto) {
        try {
            if (Pattern.matches("(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{6,20}",
                    utenteRegistrazioneDto.getPassword())) {

                if (utenteService.existUser(utenteRegistrazioneDto.getEmail())) {
                    return Response.status(Response.Status.BAD_REQUEST).build();
                }
                System.out.println(utenteRegistrazioneDto.getEmail());
                utenteService.userRegistration(utenteRegistrazioneDto);
                return Response.ok().build();
            } else {
                return Response.status(Response.Status.BAD_REQUEST).build();
            }
        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @PUT
    @Path("/update")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response userUpdate(@Valid @RequestBody UtenteAggiornamentoDto utenteAggiornamentoDto) {
        try {
            utenteService.updateUserData(utenteAggiornamentoDto);

            return Response.ok().build();

        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response userLogin(@RequestBody UtenteLoginRequestDto utenteLoginRequestDto) {
        try {
            if (utenteService.userLogin(utenteLoginRequestDto)) {
                return Response.ok(issueToken(utenteLoginRequestDto.getEmail())).build();
            }
            return Response.status(Response.Status.BAD_REQUEST).build();
        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @GET
    @Path("/get/all")
    @Secured(role = "Admin")
    @JWTTokenNeeded()
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUsers(@QueryParam("idRuolo") Integer idRuolo) {
        try {
            List<UtenteDto> listaUtenti = new ArrayList<>();
            if (idRuolo == null) {
                listaUtenti = utenteService.getUsers();
            } else {
                listaUtenti = utenteService.getUtentiByRuolo(idRuolo);
            }
            return Response.ok().entity(listaUtenti).build();
        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @PUT
    @Path("/{idUtente}/addRuolo/{idRuolo}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response addRuoloToUser(@PathParam("idUtente") int idUtente, @PathParam("idRuolo") int idRuolo) {
        try {
            RuoloDto ruolo = utenteService.addRuoloToUser(idUtente, idRuolo);
            return Response.status(Response.Status.OK).entity(ruolo).build();
        } catch (EntityNotFoundException e) {
            return Response.status(Response.Status.NOT_FOUND).entity(e.getMessage()).build();
        }
    }

    @PUT
    @Path("/{idUtente}/subscribe/{idCorso}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response subscribeUtente(@PathParam("idUtente") int idUtente, @PathParam("idCorso") int idCorso) {
        try {
            CorsoDto corsoDto = utenteService.subscribeUtente(idUtente, idCorso);
            return Response.status(Response.Status.OK).entity(corsoDto).build();
        } catch (EntityNotFoundException e) {
            return Response.status(Response.Status.NOT_FOUND).entity(e.getMessage()).build();
        } catch (Exception ex) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @PUT
    @Path("/{idUtente}/unsubscribe/{idCorso}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response unsubscribeUtente(@PathParam("idUtente") int idUtente, @PathParam("idCorso") int idCorso) {
        try {
            CorsoDto corsoDto = utenteService.unsubscribeUtente(idUtente, idCorso);
            return Response.status(Response.Status.OK).entity(corsoDto).build();
        } catch (EntityNotFoundException e) {
            return Response.status(Response.Status.NOT_FOUND).entity(e.getMessage()).build();
        } catch (Exception ex) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    public UtenteLoginResponseDto issueToken(String email) {

        byte[] secret = "stringachiavesegreta12345123411111".getBytes();

        Key key = Keys.hmacShaKeyFor(secret);

        Utente utente = utenteService.findByEmail(email);

        // Mappa dei claims
        Map<String, Object> map = new HashMap<>();
        map.put("nome", utente.getNome());
        map.put("cognome", utente.getCognome());
        map.put("email", email);

        List<String> ruoli = new ArrayList<>();

        for (Ruolo ruolo : utente.getRuoli()) {
            ruoli.add(ruolo.getTipoRuolo());
        }

        map.put("ruoli", ruoli);

        Date creationDate = new Date();
        Date endDate = Timestamp.valueOf(LocalDateTime.now().plusMinutes(15L));
        String JWTToken = Jwts.builder().setClaims(map)
                .setIssuer("http://localhost:8080")
                .setIssuedAt(creationDate)
                .setExpiration(endDate)
                .signWith(key)
                .compact();

        UtenteLoginResponseDto token = new UtenteLoginResponseDto();
        token.setToken(JWTToken);
        token.setTokenCreationTime(creationDate);
        token.setTtl(endDate);

        return token;
    }

}
