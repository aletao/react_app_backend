package it.corso.dto;

public class UtenteRuoloDto {
	
	private UtenteDto utente;
	
	private RuoloDto ruolo;

	public UtenteDto getUtente() {
		return utente;
	}

	public void setUtente(UtenteDto utente) {
		this.utente = utente;
	}

	public RuoloDto getRuolo() {
		return ruolo;
	}

	public void setRuolo(RuoloDto ruolo) {
		this.ruolo = ruolo;
	}
	
	

}
