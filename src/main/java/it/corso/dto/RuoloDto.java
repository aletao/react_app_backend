package it.corso.dto;

public class RuoloDto {
	
	public RuoloDto() {
		super();
	}
	
	public RuoloDto(Integer id, String tipoRuolo) {
		super();
		this.id = id;
		this.tipoRuolo = tipoRuolo;
	}

	private Integer id;
	
    private String tipoRuolo;

	public String getTipoRuolo() {
		return tipoRuolo;
	}

	public void setTipoRuolo(String tipoRuolo) {
		this.tipoRuolo = tipoRuolo;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	} 
    
	
}
