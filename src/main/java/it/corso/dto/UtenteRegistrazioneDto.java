package it.corso.dto;

import jakarta.validation.constraints.Pattern;

/*
 * nome, cognome, email, pw
 */
public class UtenteRegistrazioneDto {
    
    @Pattern(regexp = "[a-zA-Z]{1,50}", message = "Input non valido")
    private String nome;

    @Pattern(regexp = "[a-zA-Z]{1,50}", message = "Input non valido")
    private String cognome;

    @Pattern(regexp = "[a-zA-Z0-9\\.\\+_-]+@[a-zA-Z0-9\\._-]+\\.[a-zA-Z]{2,8}", message = "email non valida")
    private String email;

    private String password;


    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return this.cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }



}
