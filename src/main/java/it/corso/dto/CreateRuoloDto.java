package it.corso.dto;

public class CreateRuoloDto {
	
	private String tipologia;

	public String getTipologia() {
		return tipologia;
	}

	public void setTipologia(String tipologia) {
		this.tipologia = tipologia;
	}

	
}
