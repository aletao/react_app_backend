package it.corso.dto;

import jakarta.validation.constraints.Pattern;

public class UtenteLoginRequestDto {
    
    @Pattern(regexp = "[a-zA-Z0-9\\.\\+_-]+@[a-zA-Z0-9\\._-]+\\.[a-zA-Z]{2,8}", message = "formato errato")
    private String email;

    private String password;


    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
