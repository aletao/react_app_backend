package it.corso.dto;

import java.util.Date;

public class UtenteLoginResponseDto {

    private String token;

    private Date ttl;

    private Date tokenCreationTime;


    public String getToken() {
        return this.token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getTtl() {
        return this.ttl;
    }

    public void setTtl(Date ttl) {
        this.ttl = ttl;
    }

    public Date getTokenCreationTime() {
        return this.tokenCreationTime;
    }

    public void setTokenCreationTime(Date tokenCreationTime) {
        this.tokenCreationTime = tokenCreationTime;
    }

    
}
