package it.corso.service;

import java.util.List;

import it.corso.dto.UtenteDto;
import it.corso.dto.CorsoDto;
import it.corso.dto.RuoloDto;
import it.corso.dto.UtenteAggiornamentoDto;
import it.corso.dto.UtenteLoginRequestDto;
import it.corso.dto.UtenteRegistrazioneDto;
import it.corso.exceptions.EntityNotFoundException;
import it.corso.model.Utente;

public interface UtenteService {

    void userRegistration(UtenteRegistrazioneDto utenteDto);

    boolean existUser(String email);

    void updateUserData(UtenteAggiornamentoDto utente);
    
    boolean userLogin(UtenteLoginRequestDto utenteLoginRequestDto);
    
    Utente findByEmail(String email);
    
    List<UtenteDto> getUsers();
    
    RuoloDto addRuoloToUser(int id_utente, int id_ruolo) throws EntityNotFoundException;
    
    CorsoDto subscribeUtente(int idUtente, int idCorso) throws EntityNotFoundException;
    
    CorsoDto unsubscribeUtente(int idUtente, int idCorso) throws EntityNotFoundException;
    
    List<UtenteDto> getUtentiByRuolo(int idRuolo);
    /* 
    UtenteDto getUserByEmail(String email);

    void deleteUser(String email);*/


}
