package it.corso.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.corso.dto.CategoriaDto;
import it.corso.dto.CreateCategoriaDto;
import it.corso.model.Categoria;
import it.corso.repositories.CategoriaRepository;

@Service
public class CategoriaServiceImpl implements CategoriaService{
	
	@Autowired
	private CategoriaRepository categoriaRepository;
	
	@Override
	public CategoriaDto createCategoria(CreateCategoriaDto categoria) {
		
		Categoria categoriaNew = new Categoria();
		categoriaNew.setNomeCategoria(categoria.getNomeCategoria());
		categoriaNew = categoriaRepository.save(categoriaNew);
		return new CategoriaDto(categoriaNew.getId(), categoriaNew.getNomeCategoria());
	}

}