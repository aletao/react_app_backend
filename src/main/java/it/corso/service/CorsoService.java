package it.corso.service;

import java.util.List;

import it.corso.dto.CorsoDto;
import it.corso.dto.CreateCorsoDto;
import it.corso.dto.UtenteDto;
import it.corso.exceptions.EntityNotFoundException;

/*
 *CorsoDto getCourseById(int id);
 Corso createCorso(CorsoDto corso);
 List<CorsoDto> getCourses();
 void updateCourse(CorsoDtoAggiornamento corso);
 void deleteCourse(int id); 
 */

public interface CorsoService {
	
	CorsoDto createCorso(CreateCorsoDto corso);
	
	List<UtenteDto> getUtenti(Integer idCorso, Integer idRuolo) throws EntityNotFoundException;
	
	List<CorsoDto> findCorsiByIdCategoria(Integer idCategoria);

	List<CorsoDto> getCorsi();
    
}
