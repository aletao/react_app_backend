package it.corso.service;

import java.util.List;

import it.corso.dto.CreateRuoloDto;
import it.corso.dto.RuoloDto;

public interface RuoloService {

	RuoloDto createRuolo(CreateRuoloDto ruoloDto);
	
	public List<RuoloDto> getAll();
	
}
