package it.corso.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import org.apache.commons.codec.digest.DigestUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.corso.dto.CorsoDto;
import it.corso.dto.RuoloDto;
import it.corso.dto.UtenteAggiornamentoDto;
import it.corso.dto.UtenteDto;
import it.corso.dto.UtenteLoginRequestDto;
import it.corso.dto.UtenteRegistrazioneDto;
import it.corso.exceptions.EntityNotFoundException;
import it.corso.model.Corso;
import it.corso.model.Ruolo;
import it.corso.model.Utente;
import it.corso.repositories.CorsoRepository;
import it.corso.repositories.RuoloRepository;
import it.corso.repositories.UserRepository;

@Service
public class UtenteServiceImp implements UtenteService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RuoloRepository ruoloRepository;

	@Autowired
	private CorsoRepository corsoRepository;

	private ModelMapper mapper = new ModelMapper();

	@Override
	public void userRegistration(UtenteRegistrazioneDto utenteDto) {
		try {
			Utente utente = mapper.map(utenteDto, Utente.class);

			String sha256hex = DigestUtils.sha256Hex(utente.getPassword());
			utente.setPassword(sha256hex);

			userRepository.save(utente);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public boolean existUser(String email) {
		return userRepository.existsByEmail(email);
	}

	@Override
	public void updateUserData(UtenteAggiornamentoDto utenteAggiornamentoDto) {

		Utente utenteDb = userRepository.findByEmail(utenteAggiornamentoDto.getEmail());

		if (utenteDb != null) {

			utenteDb.setNome(utenteAggiornamentoDto.getNome());
			utenteDb.setCognome(utenteAggiornamentoDto.getCognome());
			utenteDb.setEmail(utenteAggiornamentoDto.getEmail());

			List<Ruolo> ruoliUtente = new ArrayList<>();
			Optional<Ruolo> ruoloDb = ruoloRepository.findById(utenteAggiornamentoDto.getIdRuolo());

			if (ruoloDb.isPresent()) {

				Ruolo ruolo = ruoloDb.get();
				ruolo.setId(utenteAggiornamentoDto.getIdRuolo());

				ruoliUtente.add(ruolo);
				utenteDb.setRuoli(ruoliUtente);

			}
			userRepository.save(utenteDb);
		}
	}

	@Override
	public boolean userLogin(UtenteLoginRequestDto utenteLoginRequestDto) {

		Utente utente = mapper.map(utenteLoginRequestDto, Utente.class);

		String sha256hex = DigestUtils.sha256Hex(utente.getPassword());
		utente.setPassword(sha256hex);

		Utente credenzialiUtente = userRepository.findByEmailAndPassword(utente.getEmail(), sha256hex);

		return credenzialiUtente != null ? true : false;
	}

	@Override
	public Utente findByEmail(String email) {
		return userRepository.findByEmail(email);
	}

	@Override
	public List<UtenteDto> getUsers() {
		List<Utente> utenti = userRepository.findAll();
		List<UtenteDto> utentiDto = new ArrayList<>();

		utenti.forEach(u -> utentiDto.add(mapper.map(u, UtenteDto.class)));
		return utentiDto;
	}

	@Override
	public RuoloDto addRuoloToUser(int id_utente, int id_ruolo) throws EntityNotFoundException{
		Optional<Utente> utente = userRepository.findById(id_utente);
		
		if(!utente.isPresent()) {
			throw new EntityNotFoundException("Nessun utente trovato con ID " + id_utente);
		} else {
			Optional<Ruolo> ruolo = ruoloRepository.findById(id_ruolo);
			if(!ruolo.isPresent()) {
				throw new EntityNotFoundException("Nessun ruolo trovato con ID " + id_ruolo);
			} else {
				Utente utenteTrovato = utente.get();
				utenteTrovato.getRuoli().add(ruolo.get());
				
				userRepository.save(utenteTrovato);
				return new RuoloDto(id_ruolo, ruolo.get().getTipoRuolo());
			}
		} 
	}
	
	@Override
	public CorsoDto subscribeUtente(int idUtente, int idCorso) throws EntityNotFoundException{
		Optional<Utente> utente = userRepository.findById(idUtente);
		Optional<Corso> corso = corsoRepository.findById(idCorso);
		
		if(!utente.isPresent()) {
			throw new EntityNotFoundException("Nessun utente trovato con ID " + idUtente);
		}
		if(!corso.isPresent()) {
			throw new EntityNotFoundException("Nessun utente trovato con ID " + idUtente);
		}

		Utente utenteDb = utente.get();

		utenteDb.getCorsi().add(corso.get());

		utenteDb = userRepository.save(utenteDb);

		return mapper.map(corso.get(), CorsoDto.class);
	} 
	
	@Override
	public CorsoDto unsubscribeUtente(int idUtente, int idCorso) throws EntityNotFoundException{
		Optional<Utente> utente = userRepository.findById(idUtente);
		Optional<Corso> corso = corsoRepository.findById(idCorso);
		
		if(!utente.isPresent()) {
			throw new EntityNotFoundException("Nessun utente trovato con ID " + idUtente);
		}
		if(!corso.isPresent()) {
			throw new EntityNotFoundException("Nessun utente trovato con ID " + idUtente);
		}

		Utente utenteDb = utente.get();

		utenteDb.setCorsi(utenteDb.getCorsi().stream().filter(c -> c.getId() != idCorso).toList());

		utenteDb = userRepository.save(utenteDb);

		return mapper.map(corso.get(), CorsoDto.class);
	} 
	
	@Override
	public  List<UtenteDto> getUtentiByRuolo(int idRuolo){
		List<Utente> listaUtenti = userRepository.findByRuoliIdIn(new HashSet<>(Arrays.asList(idRuolo)));
		List<UtenteDto> utentiDto = listaUtenti.stream().map(u -> mapper.map(u, UtenteDto.class)).toList();
		return utentiDto;
	}


}
