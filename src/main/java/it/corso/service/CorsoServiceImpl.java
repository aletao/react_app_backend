package it.corso.service;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.corso.dto.CorsoDto;
import it.corso.dto.CreateCorsoDto;
import it.corso.dto.UtenteDto;
import it.corso.exceptions.EntityNotFoundException;
import it.corso.model.Corso;
import it.corso.model.Utente;
import it.corso.repositories.CorsoRepository;

@Service
public class CorsoServiceImpl implements CorsoService {

	@Autowired
	private CorsoRepository corsoRepository;

	private ModelMapper modelMapper = new ModelMapper();

	@Override
	public CorsoDto createCorso(CreateCorsoDto createCorsoDto) {
		Corso corsodb = new Corso();

		corsodb = modelMapper.map(createCorsoDto, Corso.class);

		corsodb.getUtenti().add(new Utente(createCorsoDto.getIdDocente()));

		corsodb = corsoRepository.save(corsodb);

		return modelMapper.map(corsodb, CorsoDto.class);
	}

	@Override
	public List<UtenteDto> getUtenti(Integer idCorso, Integer idRuolo) throws EntityNotFoundException {
		Optional<Corso> corso = corsoRepository.findById(idCorso);
		if (!corso.isPresent()) {
			throw new EntityNotFoundException("Corso non trovato");
		}
		if(idRuolo != null) {
			return corso.get().getUtenti().stream()
					.filter(u -> u.getRuoli().stream().map(r -> r.getId()).toList().contains(idRuolo))
					.map(u -> modelMapper.map(u, UtenteDto.class)).toList();
		} else {
			return corso.get().getUtenti().stream().map(u -> modelMapper.map(u, UtenteDto.class)).toList();
		}
	}
	
	@Override
	public List<CorsoDto> findCorsiByIdCategoria(Integer idCategoria){
		List<Corso> corsi = corsoRepository.findByCategoriaId(idCategoria);
		return corsi.stream().map(c -> modelMapper.map(c, CorsoDto.class)).toList();
	}

	@Override
	public List<CorsoDto> getCorsi() {
		List<Corso> corsi = corsoRepository.findAll();
		return corsi.stream().map(c -> modelMapper.map(c, CorsoDto.class)).toList();
	}

}
