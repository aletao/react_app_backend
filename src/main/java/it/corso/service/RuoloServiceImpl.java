package it.corso.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.corso.dto.CreateRuoloDto;
import it.corso.dto.RuoloDto;
import it.corso.model.Ruolo;
import it.corso.repositories.RuoloRepository;

@Service
public class RuoloServiceImpl implements RuoloService {

	@Autowired
	private RuoloRepository ruoloRepository;

	@Override
	public RuoloDto createRuolo(CreateRuoloDto ruoloDto) {
		Ruolo ruolo = new Ruolo();
		ruolo.setTipoRuolo(ruoloDto.getTipologia());
		ruolo = ruoloRepository.save(ruolo);

		return new RuoloDto(ruolo.getId(), ruolo.getTipoRuolo());
	}

	@Override
	public List<RuoloDto> getAll() {
		List<Ruolo> listRuoli = ruoloRepository.findAll();
		
		List<RuoloDto> listRuoliDto = new ArrayList<>();
		
		listRuoli.forEach(r -> listRuoliDto.add(new RuoloDto(r.getId(), r.getTipoRuolo())));
		
		return listRuoliDto;
	}

}
