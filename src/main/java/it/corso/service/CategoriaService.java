package it.corso.service;

import it.corso.dto.CategoriaDto;
import it.corso.dto.CreateCategoriaDto;

public interface CategoriaService {
	
	CategoriaDto createCategoria(CreateCategoriaDto categoria);
	
}
