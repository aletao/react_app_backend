package it.corso.repositories;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import it.corso.dto.UtenteDto;
import it.corso.model.Utente;

public interface UserRepository extends JpaRepository<Utente, Integer> {
    
    boolean existsByEmail(String email);

    Utente findByEmail(String email);
    
    Utente findByEmailAndPassword(String email, String password);
    
    List<Utente> findByRuoliIdIn(Set<Integer> idRuoli);

}
