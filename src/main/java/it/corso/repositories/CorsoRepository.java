package it.corso.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import it.corso.model.Corso;

public interface CorsoRepository extends JpaRepository<Corso, Integer>{
	
	List<Corso> findByCategoriaId(int idCategoria);
    
}
