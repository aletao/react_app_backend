package it.corso.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import it.corso.model.Categoria;

public interface CategoriaRepository extends JpaRepository<Categoria, Integer>{
    
	 
}
