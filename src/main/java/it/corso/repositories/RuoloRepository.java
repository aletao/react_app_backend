package it.corso.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import it.corso.model.Ruolo;

public interface RuoloRepository extends JpaRepository<Ruolo, Integer>{
    
    
}
