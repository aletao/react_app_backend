package it.corso;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import it.corso.jwt.JWTTOkenNeededFilter;
import jakarta.ws.rs.ApplicationPath;

@Component
@ApplicationPath("/api")
public class JerseyConfig extends ResourceConfig{

    public JerseyConfig(){
    	
    	register(JWTTOkenNeededFilter.class);
        register(CorsFilter.class);
    	
        packages("it.corso.controller");
    }

}
